title: The Zebra
cover_image: /images/blogimages/thezebra_cover.jpg
cover_title: |
  How The Zebra achieved secure pipelines in black and white
cover_description: |
  The Zebra, an online insurance comparison site, adopted GitLab for SCM, CI/CD, and SAST and DAST.
twitter_image: /images/blogimages/thezebra_cover.jpg
twitter_text: Learn how @TheZebraCo uses GitLab for SCM, CI/CD, SAST and DAST.
customer_logo: /images/case_study_logos/thezebra_logo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Austin, TX
customer_solution: GitLab SaaS Ultimate
customer_employees: 350 company-wide
customer_overview: |
  The Zebra adopted GitLab to replace GitHub and Jenkins for code repository, CI/CD, and security.
customer_challenge: |
  The Zebra was looking for a solution to empower developers to own their CI/CD workflow without having to manage a variety of plug-ins.

key_benefits: >-
    
    Enhanced CI/CD 
    
    
    Less software maintenance
    
   
    Improved developer workflow
    
   
    Faster deployments
    
   
    Early security checks
    
   
    A single modernized platform
    
    
    Manageable repository
customer_stats:
  - stat: 50-75  
    label: Unknown vulnerabilities found using GitLab
  - stat: 1 tool      
    label: Down from 3 tools
  - stat: 2x  
    label: Deployment 
customer_study_content:
  - title: the customer
    subtitle: Insurance in black and white
    content: >-
        The Zebra was established to provide customers with a simplified way to compare insurance providers. Established in 2012, The Zebra is an online insurance comparison shop that researches car insurance options and delivers the best rates available. [The Zebra](https://www.thezebra.com/about/) has recently expanded into homeowners and renters insurance.

  - title: the challenge
    subtitle:  Too many plug-ins without any advantages
    content: >-
        The Zebra was using GitHub as its repository and Jenkins for deployments. The teams were also using Terraform to deploy to AWS. The number of Jenkins plugins created an overwhelming amount of management work. On top of that, the variety of plugins caused security vulnerabilities because some tools were no longer supported or too fragile to update in the deployment environment.  
       

        “The biggest problem that we were having was that we were using Jenkins to do our deploys before GitLab. We put enough plugins into that. It was so fragile that nobody wanted to touch it,” said Dan Bereczki, Sr. Software Manager. “Anybody that did try to touch it broke it, and then deploys were down for half a day, or a day to try to fix things, or keep things all upgraded.”
        

        Teams wanted to improve the existing CI/CD process, but that meant adding plugins to Jenkins, further complicating the existing level of maintenance. The Zebra needed a new solution that would integrate testing and security, as well as allow for deploys to a [variety of different platforms](/partners/).
   
  - title: the solution
    subtitle:  A fast migration with zero plug-ins
    content: >-
        The Zebra researched a variety of platforms to replace the existing plugins and ease management stress. They adopted GitLab because it provides an enhanced repository without having to manage plugins. Moreover, the CI/CD capabilities were the selling point. 
        
        
        On top of that, the teams were eager to adopt GitLab because it offers features that other solutions don’t offer, like built-in security. “People realized how much more control they had of their processes, and how easy it was to make the transition. We got the migration done in under three months,” Bereczki said. 95% of Jenkins code migrated in that time and they have since completely moved off of Jenkins and GitHub.
        
        
        All six of the application development teams and even a few other teams outside of development are using GitLab. “Now instead of one or two people who understand the intricacies of Jenkins and can fix the things that are problematic, everybody knows how to work with the GitLab pipeline,” Bereczki said. The teams went from using 3 tools - GitHub, Codeship CI, and Jenkins Deploy - to using only [GitLab CI/CD](https://about.gitlab.com/features/continuous-integration/), fully integrated and fully automated.
  
  - title: the results
    subtitle: One platform, many solutions
    content: >-
        With GitLab, Zebra can now focus on moving towards continuous deployment because teams can deploy at will, without waiting for other schedules. All of the development teams have a greater role in the deployment process because they understand how the CI pipeline works and can work within it. On top of that, the infrastructure is no longer a bottleneck for deployment.
        

        The workflow usually begins with a request from marketing. From there, it becomes a technical brief which gets broken into a set of JIRA tickets and then assigned to the appropriate team. It then gets worked on, code gets generated and goes into the GitLab repository. Then, the team will use the GitLab CI/CD pipeline to get it deployed in the development environment.  Terraform is used to implement infrastructure as code to ensure configuration changes are maintained throughout the test and deployment process.
        

        Teams use Amazon EKS with RDS. Traffic routing is initially handled by Cloudflare, then internal Elastic Load Balancing. When developers need to connect  The Zebra services to outside, 3rd party services  they use Amazon Virtual Private Cloud. “We don't want systems where it's this black box that nobody knows how it works. We're slowly getting rid of that,” Bereczki said.
       

        GitLab has enabled cross-functional relationships between the development teams because now they own their own code all the way into production. Developers can understand each step to deployment and can work through any issues and make changes without worrying about disrupting other parts of the workflow.
        

        [GitLab SAST and DAST](/solutions/dev-sec-ops/) makes it easier for compliance for SOC2 Type 1 certification and now the teams are in the middle of SOC2 Type 2 certification. It has also provided additional testing and security measures reducing their risk. “The biggest impact is, we've got a whole bunch of vulnerabilities that we didn't even know were there, that we're dealing with right now. We're remediating those,” Bereczki states. They have eliminated all identified Criticals and Highs on four projects so far. “The nice thing about it is that, since it's part of the pipeline now, we won't be playing catch up for when we schedule a penetration test, or when we run some quarterly or biannual tests,” added Bereczki.


        ## Learn more about GitLab solutions
        

        [DevSecOps with GitLab](/solutions/dev-sec-ops/)


        [CI with GitLab](/features/continuous-integration/)


        [Choose a plan that suits your needs](/pricing/)

customer_study_quotes:
  - blockquote: The biggest value (of GitLab) is that it allows the development teams to have a greater role in the deployment process. Previously only a few people really knew how things worked, and now pretty much the whole development organization knows how the CI pipeline works, can work with it, add new services, and get things into production without infrastructure being the bottleneck.
    attribution: Dan Bereczki
    attribution_title: Sr. Software Manager, The Zebra
  - blockquote: We've gone from deploying once a week, to twice a week. We're getting to a place where we're getting comfortable with our testing verification. We'll be at continuous deployment hopefully by the end of the year and be able to deploy at will.
    attribution: Dan Bereczki
    attribution_title: Sr. Software Manager, The Zebra
